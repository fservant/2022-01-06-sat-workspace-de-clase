from django.test import TestCase
from django.test import Client

from cms.models import Contenido

class GetTests (TestCase):
    def setUp(self):
        Contenido.objects.create(clave="page", valor="a page")
        Contenido.objects.create(clave="eltiempo", valor="Pagina de el tiempo")
        Contenido.objects.create(clave="deportes", valor="Aqui veras los deportes")

    def test_root(self):
        c = Client()
        response = c.get('/cms/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<title>Este es el titulo de la web</title>', content)

    def test_eltiempo(self):
        c = Client()
        response = c.get('/cms/eltiempo')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Pagina de el tiempo', content)

    def test_noexiste(self):
        c = Client()
        response = c.get('/cms/realmadrid')
        content = response.content.decode('utf-8')
        self.assertIn("No existe la llave realmadrid", content)

    def test_create(self):
        item_content = "Este es el contenido de las noticias."
        c = Client()
        response = c.post('/cms/noticias', {'content': item_content})
        self.assertEqual(response.status_code, 200)
        response = c.get('/cms/noticias')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn(item_content, content)

    def test_model_contents(self):
        Contenido.objects.create(clave="otro-deportes", valor="Aqui veras deportes")
        self.assertEqual(Contenido.objects.count(), 4)
