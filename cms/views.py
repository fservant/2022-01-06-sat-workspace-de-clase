from django.contrib.auth import logout
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt
from django.template import loader

formulario = """
<form action="" method="POST">
Introduce el valor: <input type="text" name="valor">
<br>
<input type="submit" value="Enviar">
</form>
"""

# Create your views here.
@csrf_exempt
def get_content(request, llave):

    if (request.method == "POST"):
        valor = request.body.decode('utf-8')
        c = Contenido(clave=llave, valor=valor)
        c.save()

    try:
        contenido_recuperado = Contenido.objects.get(clave=llave)
    except Contenido.DoesNotExist:
        respuesta = "No existe la llave " + llave + "<br>"

        if request.user.is_authenticated:
            respuesta = respuesta + formulario
        else:
            respuesta = respuesta + "Si quieres añadirla, por favor <a href='/login'>identifícate</a>"

        return HttpResponse(respuesta)

    respuesta = "La llave " + contenido_recuperado.clave + \
                " tiene el valor " + contenido_recuperado.valor + \
                " y el id " + str(contenido_recuperado.id)

    return HttpResponse(respuesta)

def index(request):
    content_list = Contenido.objects.all()
    template = loader.get_template('cms/index.html')
    context = {
        'content_list': content_list
    }
    return HttpResponse(template.render(context, request))

def loggedIn(request):
    if request.user.is_authenticated:
        respuesta = "Logged in as " + request.user.username
    else:
        respuesta = "No estas autenticado. <a href='/login'>Autenticate</a>"
    return HttpResponse(respuesta)

def logout_view(request):
    logout(request)
    return redirect("/cms/")

def imagen(request):
    template = loader.get_template('cms/plantilla.html')
    context = {
    }
    return HttpResponse(template.render(context, request))
