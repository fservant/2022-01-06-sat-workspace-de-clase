from django.db import models

# Create your models here.
class Contenido(models.Model):
    clave = models.CharField(max_length=64)
    valor = models.TextField(null = True)

    def __str__(self):
        return "<ID=" + str(self.id) + ", clave=" + self.clave + ", valor=" + self.valor + ">"

class Comentario(models.Model):
    usuario = models.CharField(max_length=64)
    fecha = models.DateTimeField(null = True)
    texto = models.TextField(null = True)
    contenido_referido = models.ForeignKey(Contenido, on_delete=models.CASCADE)

    def __str__(self):
        return "<ID=" + str(self.id) + ", usuario=" + self.usuario + ", fecha=" + str(self.fecha) + ", texto=" + str(self.texto) + ", contenido-referido=" + str(self.contenido_referido) + ">"

    def dice_menos(self):
        return ('menos' in self.texto)
