from django.contrib import admin

# Register your models here.
from .models import Contenido, Comentario

admin.site.register(Contenido)
admin.site.register(Comentario)
