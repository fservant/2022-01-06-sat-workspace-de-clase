from django.apps import AppConfig


class MyfirstappsatConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'myfirstappsat'
