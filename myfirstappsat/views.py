from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

def index(request):
	return HttpResponse('<h1>Welcome to my App!</h1>')
	
def say_hello(request):
	return HttpResponse('Hello!')
	
def say_bye_to(request, name):
	return HttpResponse('Bye %s'%name)
	
def say_number(request, number=0):
	return HttpResponse('Number: %s'%number)
	
